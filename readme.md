## JAVA SDK 接入指南 & CHANGELOG

- JAVA version >= 1.6
- 创建Config配置类，填入key，secret和isSandbox 参数
- 使用sdk提供的接口进行开发调试
- 上线前将Config中isSandbox值设为false以及填入正式环境的key和secret

## 基本用法
> 获取Token
```java
package cn.huolala.openapi;

import cn.huolala.openapi.sdk.HuolalaResult;
import cn.huolala.openapi.sdk.config.Config;
import cn.huolala.openapi.sdk.oauth.OAuthClient;
import cn.huolala.openapi.sdk.oauth.response.TokenResponse;
import org.junit.Before;
import org.junit.Test;

/**
 * @author bright
 * @date 2021/3/2 6:24 下午
 * @description
 */
public class OAuthClientDemoTest {
    /**
     * 设置是否沙箱环境
     */
    private static final boolean SANDBOX = true;
    /**
     * 设置APP KEY
     */
    private static final String KEY = "xxxxx";
    /**
     * 设置APP SECRET
     */
    private static final String SECRET = "xxxxxxx";


    private static OAuthClient client;

    @Before
    public void init() {
        Config config = new Config(SANDBOX, KEY, SECRET);
        client = new OAuthClient(config);
    }

    /**
     * 获取授权URL
     */
    @Test
    public void testGetOAuthUrl() {
        String redirectUri = "https://test.huolala.cn";
        String authURL = client.getAuthURL(redirectUri);
        System.out.println(authURL);
    }

    /**
     * 根据code获取token
     */
    @Test
    public void testGetToken() {
        String code = "xxxxxxxxx";
        HuolalaResult<TokenResponse> result = client.getTokenByCode(code);
        System.out.println(result);
    }

    /**
     * 通过refreshToken刷新token
     */
    @Test
    public void testGetRefreshToken() {
        String refreshToken = "";
        HuolalaResult<TokenResponse> result = client.getTokenByRefreshToken(refreshToken);
        System.out.println(result);
    }
}
```
> 调用接口
```java
package cn.huolala.openapi;

import cn.huolala.openapi.sdk.HuolalaResult;
import cn.huolala.openapi.sdk.api.service.EPService;
import cn.huolala.openapi.sdk.config.Config;
import cn.huolala.openapi.sdk.exception.ServiceException;
import cn.huolala.openapi.sdk.oauth.response.TokenResponse;
import cn.huolala.openapi.sdk.utils.JacksonUtils;
import org.junit.Before;
import org.junit.Test;

public class APIInvokerTest {

    private Config config;
    private TokenResponse token;

    @Before
    public void init() {
        config = new Config(false, "XXX", "XXX");
        token = new TokenResponse();
        token.setAccessToken("XXXX");
    }

    @Test
    public void testDetail() {
        EPService service = new EPService(config, token);
        try {
            HuolalaResult<Object> result = service.orderDetail("12345");
            System.out.println(JacksonUtils.obj2json(result));
        } catch (ServiceException e) {
            e.printStackTrace();
        }
    }
}

```