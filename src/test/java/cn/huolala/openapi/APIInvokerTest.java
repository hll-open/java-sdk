package cn.huolala.openapi;

import cn.huolala.openapi.sdk.HuolalaResult;
import cn.huolala.openapi.sdk.api.service.EPService;
import cn.huolala.openapi.sdk.config.Config;
import cn.huolala.openapi.sdk.exception.ServiceException;
import cn.huolala.openapi.sdk.oauth.response.TokenResponse;
import cn.huolala.openapi.sdk.utils.JacksonUtils;
import org.junit.Before;
import org.junit.Test;

public class APIInvokerTest {

    private Config config;
    private TokenResponse token;

    @Before
    public void init() {
        config = new Config(false, "XXX", "XXX");
        token = new TokenResponse();
        token.setAccessToken("XXXX");
    }

    @Test
    public void testDetail() {
        EPService service = new EPService(config, token);
        try {
            HuolalaResult<Object> result = service.orderDetail("12345");
            System.out.println(JacksonUtils.obj2json(result));
        } catch (ServiceException e) {
            e.printStackTrace();
        }
    }
}
