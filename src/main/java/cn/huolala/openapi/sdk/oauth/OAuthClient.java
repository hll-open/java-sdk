package cn.huolala.openapi.sdk.oauth;

import cn.huolala.openapi.sdk.HuolalaResult;
import cn.huolala.openapi.sdk.config.Config;
import cn.huolala.openapi.sdk.oauth.impl.DefaultIOAuthClient;
import cn.huolala.openapi.sdk.oauth.request.ServerRefreshTokenRequest;
import cn.huolala.openapi.sdk.oauth.request.ServerTokenRequest;
import cn.huolala.openapi.sdk.oauth.response.TokenResponse;
import cn.huolala.openapi.sdk.utils.Constants;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

public class OAuthClient {
    private final Config config;
    private IOAuthClient client;

    public OAuthClient(Config config) {
        this.config = config;
    }

    /**
     * 获取授权URL
     *
     * @param redirectUri 获取code的重定向跳转地址
     * @return 返回授权url
     */
    public String getAuthURL(String redirectUri) {
        String oauthCodeUrl = String.format(config.getServerUrl(),"#/" + Constants.OAUTH_AUTHORIZE);
        String decodeRedirectUri;
        try {
            decodeRedirectUri = URLDecoder.decode(redirectUri, "UTF-8");
            return String.format("%s&response_type=code&client_id=%s&redirect_uri=%s", oauthCodeUrl,
                    config.getAppKey(), URLEncoder.encode(decodeRedirectUri, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * 获取Token
     */
    public HuolalaResult<TokenResponse> getTokenByCode(String code) {
        client = new DefaultIOAuthClient(config, Constants.OAUTH_TOKEN);
        ServerTokenRequest request = new ServerTokenRequest(config);
        request.setCode(code);
        return client.execute(request);
    }

    /**
     * 刷新Token
     */

    public HuolalaResult<TokenResponse> getTokenByRefreshToken(String refreshToken) {
        client = new DefaultIOAuthClient(config, Constants.OAUTH_TOKEN);
        ServerRefreshTokenRequest request = new ServerRefreshTokenRequest(config);
        request.setRefreshToken(refreshToken);
        return client.execute(request);
    }

}
