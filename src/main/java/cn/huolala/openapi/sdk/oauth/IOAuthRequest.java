package cn.huolala.openapi.sdk.oauth;

import cn.huolala.openapi.sdk.oauth.response.ErrorResponse;
import com.fasterxml.jackson.databind.JavaType;

import java.util.Map;


public interface IOAuthRequest<T extends ErrorResponse> {
    JavaType getResponseJavaType();

    Map<String, String> getHeaderMap();

    Map<String, String> getBodyMap();
}
