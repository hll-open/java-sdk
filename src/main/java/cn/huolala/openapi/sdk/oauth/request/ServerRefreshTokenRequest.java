package cn.huolala.openapi.sdk.oauth.request;

import cn.huolala.openapi.sdk.HuolalaResult;
import cn.huolala.openapi.sdk.config.Config;
import cn.huolala.openapi.sdk.oauth.IOAuthRequest;
import cn.huolala.openapi.sdk.oauth.response.TokenResponse;
import cn.huolala.openapi.sdk.utils.JacksonUtils;
import com.fasterxml.jackson.databind.JavaType;

import java.util.HashMap;
import java.util.Map;

public class ServerRefreshTokenRequest implements IOAuthRequest<HuolalaResult<TokenResponse>> {
    private final Config config;
    private String refreshToken;

    public ServerRefreshTokenRequest(Config config) {
        this.config = config;
    }

    @Override
    public Map<String, String> getHeaderMap() {
        return null;
    }

    @Override
    public JavaType getResponseJavaType() {
        return JacksonUtils.getJavaType(HuolalaResult.class, TokenResponse.class);
    }

    @Override
    public Map<String, String> getBodyMap() {
        Map<String, String> bodyMap = new HashMap<String, String>(3);
        bodyMap.put("grant_type", "refresh_token");
        bodyMap.put("client_id", config.getAppKey());
        bodyMap.put("refresh_token", this.refreshToken);
        return bodyMap;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }
}
