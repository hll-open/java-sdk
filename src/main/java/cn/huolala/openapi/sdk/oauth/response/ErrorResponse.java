package cn.huolala.openapi.sdk.oauth.response;

import java.io.Serializable;


public class ErrorResponse implements Serializable {
    private String error;
    private String errorMessage;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
