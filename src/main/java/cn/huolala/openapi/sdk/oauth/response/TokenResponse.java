package cn.huolala.openapi.sdk.oauth.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TokenResponse extends ErrorResponse {
    @JsonProperty("access_token")
    private String accessToken;
    @JsonProperty("auth_mobile")
    private String authMobile;
    @JsonProperty("auth_end_time")
    private String expires;
    @JsonProperty("refresh_token")
    private String refreshToken;

    @Override
    public String toString() {
        return "TokenResponse{" +
                "accessToken='" + accessToken + '\'' +
                ", authMobile='" + authMobile + '\'' +
                ", expires='" + expires + '\'' +
                ", refreshToken='" + refreshToken + '\'' +
                '}';
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAuthMobile() {
        return authMobile;
    }

    public void setAuthMobile(String authMobile) {
        this.authMobile = authMobile;
    }

    public String getExpires() {
        return expires;
    }

    public void setExpires(String expires) {
        this.expires = expires;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }
}
