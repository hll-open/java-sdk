package cn.huolala.openapi.sdk.oauth.impl;

import cn.huolala.openapi.sdk.config.Config;
import cn.huolala.openapi.sdk.oauth.IOAuthClient;
import cn.huolala.openapi.sdk.oauth.IOAuthRequest;
import cn.huolala.openapi.sdk.oauth.response.ErrorResponse;
import cn.huolala.openapi.sdk.utils.Constants;
import cn.huolala.openapi.sdk.utils.JacksonUtils;
import cn.huolala.openapi.sdk.utils.WebUtils;

import java.io.IOException;


public class DefaultIOAuthClient implements IOAuthClient {
    private final String serverHostUrl;

    public DefaultIOAuthClient(Config config, String path) {
        this.serverHostUrl = String.format(config.getServerUrl(), path);
    }

    @Override
    public <T extends ErrorResponse> T execute(IOAuthRequest<T> request) {
        try {
            String respJson = WebUtils.doGet(serverHostUrl, request.getBodyMap(), Constants.CHARSET_UTF8);
            return JacksonUtils.json2pojo(respJson, request.getResponseJavaType());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
