package cn.huolala.openapi.sdk.oauth;

import cn.huolala.openapi.sdk.oauth.response.ErrorResponse;


public interface IOAuthClient {
    /**
     * 授权统一入口
     *
     * @param request
     * @param <T>
     * @return
     */
    <T extends ErrorResponse> T execute(IOAuthRequest<T> request);
}
