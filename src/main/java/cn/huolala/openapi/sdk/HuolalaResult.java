package cn.huolala.openapi.sdk;

import cn.huolala.openapi.sdk.oauth.response.ErrorResponse;

import java.io.Serializable;


public class HuolalaResult<T> extends ErrorResponse implements Serializable {
    private int ret;
    private String msg;
    private T data;

    public HuolalaResult() {
    }

    public HuolalaResult(int ret, String msg, T data) {
        this.ret = ret;
        this.msg = msg;
        this.data = data;
    }

    public int getRet() {
        return ret;
    }

    public void setRet(int ret) {
        this.ret = ret;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "HuolalaResult{" +
                "ret=" + ret +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }
}
