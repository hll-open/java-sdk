package cn.huolala.openapi.sdk.utils;

/**
 * @author bright
 * @date 2021/3/1 2:37 下午
 * @description
 */
public abstract class Constants {

    /**
     * 默认时间格式
     **/
    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    /**
     * Date默认时区
     **/
    public static final String DATE_TIMEZONE = "GMT+8";

    /**
     * UTF-8字符集
     **/
    public static final String CHARSET_UTF8 = "UTF-8";

    /**
     * HTTP请求相关
     **/
    public static final String METHOD_POST = "POST";
    public static final String METHOD_GET = "GET";
    public static final String CTYPE_FORM_DATA = "application/x-www-form-urlencoded";
    public static final String CTYPE_FILE_UPLOAD = "multipart/form-data";
    public static final String CTYPE_TEXT_XML = "text/xml";
    public static final String CTYPE_APPLICATION_XML = "application/xml";
    public static final String CTYPE_TEXT_PLAIN = "text/plain";
    public static final String CTYPE_APP_JSON = "application/json";

    /**
     * GBK字符集
     **/
    public static final String CHARSET_GBK = "GBK";

    /**
     * JSON 应格式
     */
    public static final String FORMAT_JSON = "json";
    /**
     * XML 应格式
     */
    public static final String FORMAT_XML = "xml";

    /**
     * JSON 新格式
     */
    public static final String FORMAT_JSON2 = "json2";
    /**
     * XML 新格式
     */
    public static final String FORMAT_XML2 = "xml2";

    /**
     * MD5签名方式
     */
    public static final String SIGN_METHOD_MD5 = "md5";
    /**
     * HMAC签名方式
     */
    public static final String SIGN_METHOD_HMAC = "hmac";

    public static final String OAUTH_AUTHORIZE = "oauth/authorize";

    public static final String OAUTH_TOKEN = "oauth/token";

    /**
     * 小B
     */
    public static final String USER_OPEN_API = "v1";

    /**
     * 大B
     */
    public static final String EP_OPEN_API = "api";


    public static final String OPEN_API = "v1";

    /**
     * 响应编码
     */
    public static final String ACCEPT_ENCODING = "Accept-Encoding";
    public static final String CONTENT_ENCODING = "Content-Encoding";
    public static final String CONTENT_ENCODING_GZIP = "gzip";

    /**
     * 默认媒体类型
     **/
    public static final String MIME_TYPE_DEFAULT = "application/octet-stream";

    /**
     * 默认流式读取缓冲区大小
     **/
    public static final int READ_BUFFER_SIZE = 1024 * 4;

}
