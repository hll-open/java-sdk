package cn.huolala.openapi.sdk.utils;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Converters {
    private static final Map<String, Object> fieldCache = new ConcurrentHashMap<String, Object>();
    /**
     * 空缓存，避免缓存击穿后影响Field和Method的获取性能。
     */
    private static final Object emptyCache = new Object();

    public static Field getField(Class<?> clazz, PropertyDescriptor pd) throws Exception {
        String key = clazz.getName() + "_" + pd.getName();
        Object field = fieldCache.get(key);
        if (field == null) {
            try {
                field = clazz.getDeclaredField(pd.getName());
            } catch (NoSuchFieldException e) {
                //如果当前类找不到字段，找父级类是否包含字段，直到顶级类Object
                Class<?> superClazz = clazz.getSuperclass();
                if (!"java.lang.Object".equals(superClazz.getName())) {
                    try {
                        field = superClazz.getDeclaredField(pd.getName());
                    } catch (NoSuchFieldException e1) {
                        // cache isolated field
                        field = emptyCache;
                    }
                } else {
                    // cache isolated field
                    field = emptyCache;
                }
            }
            fieldCache.put(key, field);
        }
        return field == emptyCache ? null : (Field) field;
    }
}
