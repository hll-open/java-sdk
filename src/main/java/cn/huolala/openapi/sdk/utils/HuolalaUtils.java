package cn.huolala.openapi.sdk.utils;

import cn.huolala.openapi.sdk.utils.json.JSONWriter;


public abstract class HuolalaUtils {
    /**
     * 把对象结构转换为JSON字符串。
     *
     * @param object 对象结构
     * @return JSON字符串
     */
    public static String objectToJson(Object object) {
        JSONWriter writer = new JSONWriter(false, true);
        return writer.write(object);
    }
}
