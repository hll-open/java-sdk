package cn.huolala.openapi.sdk.config;

/**
 * 日志
 */
public interface HuolalaSDKLogger {
    /**
     * level info
     *
     * @param msg msg
     */
    void info(String msg);

    /**
     * level error
     *
     * @param msg msg
     */
    void error(String msg);
}
