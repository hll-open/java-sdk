package cn.huolala.openapi.sdk.config;

import cn.huolala.openapi.sdk.utils.StringUtils;

public class Config {
    /**
     * app_key
     */
    private final String appKey;
    /**
     * app_secret
     */
    private final String appSecret;
    /**
     * 请求环境
     */
    private final boolean sandbox;
    /**
     * 服务url
     */
    private final String serverUrl;

    private final String apiServerUrl;

    public Config(boolean sandbox, String appKey, String appSecret) {
        if (StringUtils.areNotEmpty(appKey, appSecret)) {
            this.sandbox = sandbox;
            this.appKey = appKey;
            this.appSecret = appSecret;
            if (sandbox) {
                this.serverUrl = BasicURL.OAUTH_SANDBOX_SERVER;
                this.apiServerUrl = BasicURL.API_SANDBOX_SERVER;
            } else {
                this.serverUrl = BasicURL.OAUTH_PRODUCTION_SERVER;
                this.apiServerUrl = BasicURL.API_PRODUCT_SERVER;
            }
            System.out.println("Config init success");
        } else {
            throw new RuntimeException("Key or secret is not configured.");
        }
    }

    public String getApiServerUrl() {
        return apiServerUrl;
    }

    public String getAppKey() {
        return appKey;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public boolean isSandbox() {
        return sandbox;
    }

    public String getServerUrl() {
        return serverUrl;
    }

}
