package cn.huolala.openapi.sdk.config;

public abstract class BasicURL {
    /**
     * basic  pre url
     */
    public static final String OAUTH_SANDBOX_SERVER = "https://open.huolala.cn/%s?isSandbox=true";

    /**
     * basic prd url
     */
    public static final String OAUTH_PRODUCTION_SERVER = "https://open.huolala.cn/%s?isSandbox=false";


    public static final String API_PRODUCT_SERVER = "https://openapi.huolala.cn/v1";


    public static final String API_SANDBOX_SERVER = "https://openapi-pre.huolala.cn/v1";

}
