package cn.huolala.openapi.sdk.api.service;

import cn.huolala.openapi.sdk.api.BaseService;
import cn.huolala.openapi.sdk.config.Config;
import cn.huolala.openapi.sdk.oauth.response.TokenResponse;

/**
 * 用户服务
 */
public class UserService extends BaseService {
    public UserService(Config config, TokenResponse token) {
        super(config, token);
    }

    public UserService(Config config) {
        super(config);
    }
}
