package cn.huolala.openapi.sdk.api.service;

import cn.huolala.openapi.sdk.HuolalaResult;
import cn.huolala.openapi.sdk.annotation.Service;
import cn.huolala.openapi.sdk.api.BaseService;
import cn.huolala.openapi.sdk.config.Config;
import cn.huolala.openapi.sdk.exception.ServiceException;
import cn.huolala.openapi.sdk.oauth.response.TokenResponse;

import java.util.HashMap;
import java.util.Map;

/**
 * 企业服务
 */
public class EPService extends BaseService {

    public EPService(Config config, TokenResponse token) {
        super(config, token);
    }

    public EPService(Config config) {
        super(config);
    }

    @Service(apiMethod = "e-order-detail", needToken = true)
    public HuolalaResult<Object> orderDetail(String orderDisplayId) throws ServiceException {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("order_display_id", orderDisplayId);
        return callByBody(params);
    }
}
