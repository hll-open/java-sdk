package cn.huolala.openapi.sdk.api;

import cn.huolala.openapi.sdk.annotation.Service;
import cn.huolala.openapi.sdk.config.Config;
import cn.huolala.openapi.sdk.exception.ServiceException;
import cn.huolala.openapi.sdk.oauth.response.TokenResponse;
import cn.huolala.openapi.sdk.utils.WebUtils;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class BaseService {
    private final Config config;
    private final Map<String, Method> methodMap = new HashMap<String, Method>();
    private TokenResponse token;

    public BaseService(Config config, TokenResponse token) {
        this.config = config;
        this.token = token;
        Method[] methods = this.getClass().getDeclaredMethods();
        for (Method method : methods) {
            methodMap.put(method.getName(), method);
        }
    }

    public BaseService(Config config) {
        this.config = config;
    }

    protected <T> T callByBody(Map<String, Object> requestBody) throws ServiceException {
        String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
        Method method = methodMap.get(methodName);
        Service service = method.getAnnotation(Service.class);
        if (null == service) {
            throw new ServiceException("服务未找到Service注解");
        }
        return WebUtils.call(config, service, requestBody, null, token);
    }


    protected <T> T callByApiData(Map<String, Object> requestApiData) throws ServiceException {
        String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
        Method method = methodMap.get(methodName);
        Service service = method.getAnnotation(Service.class);
        if (null == service) {
            throw new ServiceException("服务未找到Service注解");
        }
        return WebUtils.call(config, service, null, requestApiData, token);
    }
}
